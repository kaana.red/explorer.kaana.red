# Pull base image.
FROM ubuntu:18.04

ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

#####################################################################################root##
### install base libraries and dependencies for faircoin daemon ###########################
RUN apt-get update -q && \
    apt-get install -qy \
        git \
        net-tools \
        curl \
        nano

#####################################################################################root##
### install python packages
RUN apt-get install -qy \
        python3-pip \
        python3-dev

RUN python3 -m pip install \
        simplejson \
        jinja2 \
        PyMySQL \
        interruptingcow \
        DBUtils \
        CherryPy \
        requests

#####################################################################################root##
### system cleanup ########################################################################
RUN rm -rf /var/lib/apt/lists/* && \
    apt-get autoremove -y && \
    apt-get clean

#####################################################################################root##
### create and run user account to image ##################################################
ARG RUNNER_GID
ARG RUNNER_UID
RUN groupadd -g $RUNNER_GID faircoin
RUN useradd --create-home --shell /bin/bash faircoin --uid $RUNNER_UID --gid $RUNNER_GID

RUN mkdir -p /home/faircoin/explorer
RUN chown -R faircoin:faircoin /home/faircoin/explorer

COPY ./scripts/entrypoint.sh /home/faircoin/explorer/scripts/entrypoint.sh
RUN chown faircoin:faircoin /home/faircoin/explorer/scripts/entrypoint.sh

USER faircoin
WORKDIR /home/faircoin/explorer/scripts

ENTRYPOINT ["/home/faircoin/explorer/scripts/entrypoint.sh"]
