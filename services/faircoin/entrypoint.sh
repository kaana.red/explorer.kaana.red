#!/usr/bin/env bash

# stop faircoind before container stops
stop_faircoind() {
  ./faircoin-cli stop > ./shutdown.log
}
trap 'stop_faircoind' SIGINT
trap 'stop_faircoind' SIGKILL
trap 'stop_faircoind' SIGTERM

if [ "${AUTOSTART}" == "1" ]
then
  ./faircoind -disablewallet -daemon -blocknotify='cd /home/faircoin/explorer/scripts && python3 dbload.py'
fi

while true; do : sleep 5; done
