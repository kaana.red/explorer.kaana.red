# FairCoin blockchain explorer

The service is made for/controlled by gitlab runner.

## Usage

### by Gitlab-Runner

1. Fork this project to your group/repositories where you have set up a gitlab-runner
1. change the gitlab-runner tags in .gitlab-ci.yml
1. add env files ( select Type = file ! ) to override defaults ./env_file , go to Gitlab **Settings** -> **CI/CD** -> **Variables**

You can read a full developer report of server preparation with gitlab-runner usage > [DEV_REPORT.md](DEV_REPORT.md)

~~~
#### env files ( examples see env_file/... / select Type = file !) ######################
CCE_CONF              # config dbload.py
SERVER_CONF           # config websrv.py
FAIRCHAINS_CONF       # conf file to run the faircoind or fairchains daemon

#### env vars ###########################################################################
MYSQL_USER            # mysql user ( used to run the explorer )
MYSQL_PASSWORD        # mysql user password
MYSQL_ROOT_PASSWORD   # mysql root user password ( used to admin the database )

LH_PORT_explorer      # port running explorer by localhost ( default 8222 / use nginx proxy to make it available by remote   )  
~~~
1. run command, go to Gitlab **CI/CD** -> **Pipeline** and **Run Pipeline**
Enter variable name **CMD**
~~~
build        # build container ( changes of Dockerfile )
init         # remove blockchain data and initialize sql database and faircoin/fairchains.conf
start        # start container ( changes of scripts )
stop         # stop container
install      # runs build, init, start stages
uninstall    # remove container without to delete the data
remove       # remove all data
removeall    # remove all data and docker images
~~~

### by SSH access <small>( manually without gitlab-runner )</small>

1. install docker and docker-compose ( https://docs.docker.com/compose/install/ )
1. clone this project
1. change configuration in ./env_file
1. Initialize env vars
~~~
chmod +x setenv
. setenv
~~~

1. run command from project root folder
~~~
deploy/build
deploy/init
deploy/start
deploy/stop
deploy/install
deploy/uninstall
deploy/remove
deploy/removeall
~~~

#### Troubleshooting

some commands can be helpful getting shell access to container or get some status info
~~~
console/explorer                    # get access to container of explorer server app
console/db                          # get access to container of mysql database
console/faircoin                    # get access to container of faircoind

console/ps                          # list containers status info
console/status                      # list all ports / processes and faircoind info
~~~
